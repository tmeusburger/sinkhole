#![feature(plugin, custom_derive)]
#![plugin(rocket_codegen)]

extern crate rocket;
#[macro_use] extern crate rocket_contrib;
extern crate serde;
extern crate serde_json;
#[macro_use] extern crate serde_derive;

#[macro_use] extern crate log;
extern crate env_logger;

use rocket_contrib::Json;
use std::ops::Deref;

#[derive(Deserialize, Serialize, Debug)]
struct TrackUpdate {
    version      : String,
    tracking     : Vec<Track>,
    order_number : Option<String>,
}

#[derive(Deserialize, Serialize, Debug)]
struct Track {
    tracking_number         : String,
    carrier_moniker         : Option<String>,
    estimated_delivery_date : Option<String>,
    service_code            : Option<String>,
    service_description     : Option<String>,
    events                  : Option<Vec<Event>>,
}

#[derive(Deserialize, Serialize, Debug)]
struct Event {
    carrier_moniker     : Option<String>,
    date                : Option<String>,
    location            : Option<Location>,
    status_code_mapping : Option<StatusCodeMapping>,
}

#[derive(Deserialize, Serialize, Debug)]
struct Location {
    city    : Option<String>,
    country : Option<String>,
    state   : Option<String>,
    zipcode : Option<String>,
}

#[derive(Deserialize, Serialize, Debug)]
struct StatusCodeMapping {
    code: Option<String>,
    code_description: Option<String>,
}


#[post("/hole", format = "application/json", data = "<track_update>")]
fn track_update(track_update: Json<TrackUpdate>) -> String {
    //println!("{:?}", track_update);
    let j = serde_json::to_string(&track_update.deref()).expect("Failed writing to log");
    warn!("{}", j);

    j
}

#[get("/health_check")]
fn health_check() -> String {
    "Up".to_string()
}

fn main() {
    rocket::ignite().mount("/", routes![health_check, track_update]).launch();
}
